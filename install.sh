#!/bin/sh 

#create a sub-set of Build Directories

cd mspgcc-build
mkdir binutils-2.22-msp430
mkdir gcc-4.7.0-msp430
mkdir gdb-7.2-msp430
 
 # Configure Binutils
 cd binutils-2.22-msp430
# We need to build binutils for the msp430
 ../binutils-2.22/configure --target=msp430 --program-prefix="msp430-" --disable-doc 
 
 make
 make install
 
 #cd ../gcc-4.7.0
 # ./contrib/download_prerequisites
 
 
 #Configure GCC
 #cd ../gcc-4.7.0-msp430
 #../gcc-4.7.0/configure --target=msp430 --enable-languages=c --program-prefix="msp430-" 
 
 #make
 #make install
 #Configure GDB
 
 #cd ../gdb-7.2-msp430
 #../gdb-7.2/configure --target=msp430 --program-prefix="msp430-" 
 
 #make
 #make install
 
 #Install the mspgcc-mcu files
 #cd ../msp430mcu-20130321
 #MSP430MCU_ROOT=`pwd` ./scripts/install.sh /usr/local/
 
 # Install the mspgcc-libc
 #cd ../msp430-libc-20120716
 #cd src
 #make 
 #make PREFIX=/usr/local install
 #cd ..
 


