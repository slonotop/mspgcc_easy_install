#!/bin/sh 
cd mspgcc-build

cd binutils-2.22
patch -p1<../mspgcc-20120911/msp430-binutils-2.22-20120911.patch

cd ../gcc-4.7.0
patch -p1<../mspgcc-20120911/msp430-gcc-4.7.0-20120911.patch 

cd ../gdb-7.2
patch -p1<../mspgcc-20120911/msp430-gdb-7.2a-20111205.patch

cd ..
