#!/bin/sh
#Install MSPGCC on ubuntu 12.04

 mkdir mspgcc
 cp install.sh mspgcc/
 cp patch.sh mspgcc/
 cd mspgcc
 sudo apt-get install git-core texinfo gcc libexpat1 libncurses5-dev  libreadline6-dev zlibc zlib1g-dev libx11-dev libusb-dev mspdebug

#wget http://sourceforge.net/projects/mspgcc/files/mspgcc/mspgcc-20120406.tar.bz2
#wget http://sourceforge.net/projects/mspgcc/files/Patches/gcc-4.7.0/msp430-gcc-4.7.0-20120911.patch
wget http://sourceforge.net/projects/mspgcc/files/mspgcc/DEVEL-4.7.x/mspgcc-20120911.tar.bz2
#wget http://sourceforge.net/projects/mspgcc/files/Patches/binutils-2.22/msp430-binutils-2.22-20120911.patch

#wget http://sourceforge.net/projects/mspgcc/files/msp430mcu/msp430mcu-20120407.tar.bz2
wget http://sourceforge.net/projects/mspgcc/files/msp430mcu/msp430mcu-20130321.tar.bz2
#wget http://sourceforge.net/projects/mspgcc/files/msp430-libc/msp430-libc-20120224.tar.bz2
wget http://sourceforge.net/projects/mspgcc/files/msp430-libc/msp430-libc-20120716.tar.bz2

wget http://ftpmirror.gnu.org/binutils/binutils-2.22.tar.bz2
wget http://ftpmirror.gnu.org/gcc/gcc-4.7.0/gcc-4.7.0.tar.bz2
wget http://ftpmirror.gnu.org/gdb/gdb-7.2a.tar.bz2

mkdir mspgcc-build

tar xfj mspgcc-20120911.tar.bz2  -C mspgcc-build/
tar xfj msp430mcu-20130321.tar.bz2  -C mspgcc-build/
tar xfj msp430-libc-20120716.tar.bz2  -C mspgcc-build/
tar xfj binutils-2.22.tar.bz2  -C mspgcc-build/
tar xfj gdb-7.2a.tar.bz2 -C mspgcc-build/
tar xfj gcc-4.7.0.tar.bz2 -C mspgcc-build/
